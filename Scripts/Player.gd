extends KinematicBody2D

# Constants
const GRAVITY = 20
const UP = Vector2(0, -1)

# Variables
export var moveSpeed = 200
export var jumpForce = -500

var motion = Vector2()

# Functions
func Movement():
	motion.y += GRAVITY
	
	if Input.is_action_pressed("right"):
		motion.x = moveSpeed
	elif Input.is_action_pressed("left"):
		motion.x = -moveSpeed
	else:
		motion.x = 0
	
	if is_on_floor():
		if Input.is_action_pressed("jump"):
			motion.y = jumpForce
	
	move_and_slide(motion, UP)
